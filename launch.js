var     CONFIG = require('./config/config.js'),
    auth = require('./server/authenticate'),
    server = require('./server/server');


if(CONFIG.authenticateTokenAtClient){
    try {
        server.startServer();

    } catch (exception) {
        console.log("ERROR STARTING SERVER");
        console.log(exception);
    }

} else {

    var token = {
        client_id:      CONFIG.client_id,
        client_secret:  CONFIG.client_secret,
        grant_type:     CONFIG.grant_type,
        scope:          CONFIG.scope,
        tenant_id:      CONFIG.tenant_id
    };


    auth.authenticateToken(token).then(function(success){
        CONFIG.token = success;
        try {
            server.startServer();
        } catch (exception) {
            console.log(exception);
        }


    }, function(error){
        console.log(error.error_description);
    });

}





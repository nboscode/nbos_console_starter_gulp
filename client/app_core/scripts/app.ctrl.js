'use strict';

/**
 * @ngdoc function
 * @name newappApp.controller:MainCtrl
 * @description
 * # AppCtrl
 * Controller of the Entire Application
 */

angular.module('app.core')
    .controller('AppCtrl', ['$scope', 'APP_CONFIG', function ($scope, APP_CONFIG) {


        /**
         *    NBOS NOTE: ENVIRONMENT VARIABLE IS IDENTIFIED HERE
         */


        APP_CONFIG.ENV = document.getElementById('ENV').innerHTML.toUpperCase();
        APP_CONFIG.ACCESS_TOKEN = document.getElementById('access_token').innerHTML;
        APP_CONFIG.TENANT_ID = document.getElementById('tenant_id').innerHTML;

    }]);

'use strict';


angular.module('app.core')
    .factory('APP_ConfigService', ['IDN_SDK', '$q', 'APP_CONFIG', 'CLIENT_CONFIG', function(IDN_SDK, $q, APP_CONFIG, CLIENT_CONFIG){
        var factory = {};

        factory.token = {};

        factory.getAppConfig = function(){
            var deferred = $q.defer();

            IDN_SDK.get_Application_Config().get({tenantId : CLIENT_CONFIG.TENANT_ID}).$promise.then(function(success){
                factory.config = success;
                APP_CONFIG.config = true;
                for(var key in success){
                    if(key[0] != "$"){
                        APP_CONFIG[key] = success[key];
                    };
                };
                deferred.resolve();
            }, function(error){
                deferred.reject(error);
            });

            return deferred.promise;

        };

        factory.getToken = function(){
            var deferred = $q.defer();
            var obj = {};
            obj.client_id = CLIENT_CONFIG.CLIENT_ID;
            obj.client_secret = CLIENT_CONFIG.CLIENT_SECRET;
            obj.grant_type  = CLIENT_CONFIG.GRANT_TYPE;
            obj.scope = CLIENT_CONFIG.SCOPE;
            obj.tenant_id = CLIENT_CONFIG.TENANT_ID;

            IDN_SDK.get_application_token().post($.param(obj)).$promise.then(function(response){

                APP_CONFIG.ACCESS_TOKEN = response.access_token;
                APP_CONFIG.TENANT_ID = obj.tenant_id;
                deferred.resolve(response);

            }, function(error){

                console.log(error);
                deferred.reject(error);

            });

            return deferred.promise;
        };

        return factory;

    }]);
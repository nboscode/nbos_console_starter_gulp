/**
 * In this file:
 *  1. Defines App Config modules
 *  2. Setup Constants required to communicate with IDN
 *  3. Setup Values to store response form IDN
 *  4. Configure default routes
 *  5. Configure localstorage
 *  6. Setup Underscore
 */

'use strict';

angular.module('app.core', []);


angular.module('app.core')

    /*
        NBOS NOTE:

        APP_CONFIG holds the configuration values of your application based on how you've configured your project on http://console.nbos.io
        SESSION_CONFIG holds all session information of the application.
     */
    .value('APP_CONFIG', {})
    .value('SESSION_CONFIG', {})


    /*
        NBOS NOTE:
        Configuring Routes Herer
     */
    .config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/home');

        $stateProvider
            .state('home', {
                url: '/home',
                templateUrl: "app_core/views/firstPage/firstPage.html",
                controller: 'FirstPageCtrl',
                data: {
                    module: "App",
                    type: 'home',
                    display : false,
                    disabled : false,
                    authenticate : false
                }
            })
            .state('error', {
                url: '/error',
                templateUrl: "app_core/views/error/error.html",
                controller: 'ErrorCtrl',
                data: {
                    module: "App",
                    type: 'home',
                    display : false,
                    disabled : false,
                    authenticate : false
                }
            })
    }]);

angular.module('app.core')

    .constant('IDN_CONSTANTS', {
        API_URL: 'http://api.qa1.nbos.in/',
        API_URL_DEV: 'http://api.qa1.nbos.in/',
        INVALID_DETAILS : 500,
        INTERNAL_ERROR : "VALIDATION ERROR"
    })

    .factory('IDN_SDK', ['IDN_CONSTANTS', 'APP_CONFIG', 'SESSION_CONFIG', '$resource', function(IDN_CONSTANTS, APP_CONFIG, SESSION_CONFIG, $resource){
        var factory = {};
        var api_url = IDN_CONSTANTS.API_URL + 'api/identity/v0/';


        factory.get_user_roles = function(){
            var bearer = "Bearer " + SESSION_CONFIG.USER_TOKEN;

            return $resource(api_url + 'tenants/:tenantId/members/:uuid/roles', {}, {
                'get':{
                    method : 'GET',
                    isArray : true,
                    headers: {
                        "Authorization" : bearer
                    }
                }
            });
        };

        factory.get_all_roles = function(){
            var bearer = "Bearer " + SESSION_CONFIG.USER_TOKEN;

            return $resource(api_url + 'tenants/:tenantId/roles', {}, {
                'get':{
                    method : 'GET',
                    isArray : true,
                    headers: {
                        "Authorization" : bearer
                    }
                }
            });

        };

        factory.get_user = function() {
            var bearer = "Bearer " + SESSION_CONFIG.USER_TOKEN;

            return $resource(api_url + 'users/:userId', {}, {
                'get': {
                    method: 'GET',
                    headers: {
                        "Authorization": bearer
                    }
                },
                'update': {
                    method: 'PUT',
                    headers: {
                        "Authorization": bearer
                    }
                }
            })
        };

        factory.get_user_image = function() {
            var bearer = "Bearer " + SessionService.getStoredUserToken();

            return $resource(MOD_NBOS.API_URL + 'media/v0/media', {}, {
                'get': {
                    method: 'GET',
                    headers: {
                        "Authorization": bearer
                    }
                }
            })
        };


        factory.get_tenant_user = function() {
            var bearer = "Bearer " + SESSION_CONFIG.USER_TOKEN;

            return $resource(api_url + 'tenants/:tenantId/members', {
                tenantId: '@tenantId'
            }, {
                'get': {
                    method: 'GET',
                    isArray: true,
                    headers: {
                        "Authorization": bearer
                    }
                }
            })
        };

        factory.update_user_image = function(id, file) {

            var deferred = $q.defer();
            var bearer = "Bearer " + SESSION_CONFIG.USER_TOKEN;
            Upload.upload({
                url: api_url + 'api/media/v0/media?id=' + id + '&mediafor=profile',
                headers: {
                    'Authorization': bearer
                },
                file: file,
            }).success(function(data, status, headers, config) {
                console.log(data);
                deferred.resolve(data);

            }).error(function(error) {
                deferred.reject();

            });

            return deferred.promise;

        };

        factory.forgot_password = function(){

            var bearer = "Bearer " + SESSION_CONFIG.USER_TOKEN;
            return $resource(api_url + 'auth/forgotPassword', {},{
                'forgot' :{
                    method : 'POST',
                    headers :{
                        "Authorization" : bearer
                    }
                }
            });
        };

        factory.update_password = function() {

            var bearer = "Bearer " + SESSION_CONFIG.USER_TOKEN;
            return $resource(api_url + 'auth/changePassword', {}, {
                'authenticate': {
                    method: 'POST',
                    headers: {
                        "Authorization": bearer
                    }
                }
            });
        };

        factory.reset_password = function(){

            var bearer = "Bearer " + SESSION_CONFIG.USER_TOKEN;
            return $resource(api_url + 'auth/forgotPassword', {},{
                'reset' :{
                    method : 'POST',
                    headers :{
                        "Authorization" : bearer
                    }
                }
            });
        };


        factory.get_user_profile = function() {
            var bearer = "Bearer " + SESSION_CONFIG.USER_TOKEN;

            return $resource(IDN_CONSTANTS.NODE_API + 'member/:userId', {
                userId: '@userId'
            }, {
                'get': {
                    method: 'GET',
                    headers: {
                        "Authorization": bearer
                    }
                },
                'update': {
                    method: 'POST',
                    headers: {
                        "Authorization": bearer
                    }
                }
            })
        };

        /**
         * User Logout
         * @returns {*}
         */

        factory.user_logout = function(){
            var bearer = "Bearer " + SESSION_CONFIG.USER_TOKEN;

            return $resource(api_url + 'auth/logout', {},{
                'logout' :{
                    method : 'GET',
                    headers :{
                        "Authorization" : bearer
                    }
                }
            });
        };

        /**
         * Authenticate a user for login
         * @param {Object} obj
         * @returns {Object} token
         */

        factory.user_authenticate = function(){

            var bearer = "Bearer " + APP_CONFIG.ACCESS_TOKEN;
            return $resource(api_url + 'auth/login', {},{
                'authenticate' :{
                    method : 'POST',
                    headers :{
                        "Authorization" : bearer
                    }
                }
            });
        };

        /**
         * Register a user.
         * @param {Object} obj
         * @param {String} obj.firstName
         * @param {String} obj.lastName
         * @param {String} obj.username
         * @param {String} obj.password
         * @returns {Object} member
         * @returns {String} member.email
         * @returns {String} member.uuid
         * @returns {String} member.firstName
         * @returns {String} member.lastName
         * @returns {Object} token
         * @returns {String} token.access_token
         * @returns {String} token.refresh_token
         * @returns {String} token.expires_in
         * @returns {String} token.token_type
         */

        factory.register_user = function(){

            var bearer = "Bearer " + APP_CONFIG.ACCESS_TOKEN;
            return $resource(api_url + 'users/signup',{}, {
                save : {
                    method :'POST',
                    headers :{
                        "Authorization" : bearer
                    }
                }
            })
        };



        factory.get_Application_Config = function(){
            // return $http.get('data/app_config.json')
            var bearer = "Bearer " + APP_CONFIG.ACCESS_TOKEN;

            return $resource(IDN_CONSTANTS.API_URL + 'api/core/v0/config/values/tenant/:tenantId', {tenantId : '@tenantId'},{
                'get' :{
                    method : 'GET',
                    headers:{
                        "Authorization" : bearer
                    }
                }
            });
        };


        /**
         * Returns application token
         * @param {Object} obj
         * @param {String} obj.client_id
         * @param {String} obj.client_secret
         * @param {String} obj.grant_type
         * @param {String} obj.scope
         * @param {String} obj.tenant_id
         * @returns {Object} token
         */
        factory.get_application_token = function(){
            return $resource(IDN_CONSTANTS.API_URL + 'oauth/token', {},{
                'post' :{
                    method : 'POST',
                    headers:{
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                }
            });
        };

        return factory;
    }]);
'use strict';
angular.module('app.core')

.controller('FirstPageCtrl', ['$scope', 'APP_ConfigService', 'APP_NavService', 'StateService', '$state', '$q', 'APP_CONFIG', 'Logger', 'ErrorService', 'CLIENT_CONFIG', function($scope, APP_ConfigService, APP_NavService, StateService, $state, $q, APP_CONFIG, Logger, ErrorService, CLIENT_CONFIG){
    $scope.message="Hello and Welcome to your App";

    $scope.showError = false;
    $scope.error = "";

    var getConfigInfo = function(){

        var config = APP_ConfigService.getAppConfig(),
            navigation = APP_NavService.createLinks();

        $q.all([config, navigation])
            .then(function(success2){
                Logger.log("App Level Configuration recieved");
                console.log(CLIENT_CONFIG)
                if(StateService.state){
                    $state.go(StateService.state.next.name, StateService.state.params);
                } else {
                    $state.go('idn.dashboard');
                };

            }, function(error2){
                ErrorService.error = error2;
                $state.go('error');
            });
    };

    /**
     *
     */

    var init = function(){
        if(!APP_CONFIG.ACCESS_TOKEN){

            APP_ConfigService.getToken().then(function(success){

                getConfigInfo();

            }, function(error){
                ErrorService.error = error;
                $state.go('error');
                //Error Authorising Token

            });



        } else if(!APP_CONFIG.config){

            getConfigInfo();

        } else {

            if(StateService.state){
                $state.go(StateService.state.next.name, StateService.state.params);
            } else {
                $state.go('idn.dashboard');
            }
        };
    };

    init();


}]);
'use strict';
angular.module('app.core')
    .controller('ErrorCtrl', ['$scope', '$state', 'APP_CONFIG', 'ErrorService', function($scope, $state, APP_CONFIG, ErrorService){

        $scope.message="Hello and Welcome to your App";
        $scope.unAuthorisedError = false;
        $scope.pageNotFoundError = false;
        $scope.showError = false;

        $scope.error = {};

        var init = function(){
            if(ErrorService.error){
                $scope.error = ErrorService.error;
                ErrorService.error = null;
            } else {
                // $state.go('home');
            };

        };

        init();

    }]);
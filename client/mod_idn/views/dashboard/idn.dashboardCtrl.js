/**
 *
 * @ ACTION 1:
 * Get all user related information here
 * Get all APP level configurations here
 * Redirect to app configured main page post login
 */
'use strict';

angular.module('mod.idn')

    .controller('IdnDashboardCtrl', ['$scope', 'SESSION_CONFIG', 'SessionService', 'UserService', 'RolesService', 'Logger', '$state', '$q', 'APP_CONFIG', 'LayoutService', 'StateService', function ($scope, SESSION_CONFIG, SessionService, UserService, RolesService, Logger, $state, $q, APP_CONFIG, LayoutService, StateService) {

        $scope.showError = false;
        $scope.showInvalidToken = false;
        $scope.error = '';


        var navigateUserToModule = function () {

            var state = StateService.state;

            if (state) {
                $state.go(state.next.name, state.params);
            } else {
                // $state.go(APP_CONFIG.MOD_IDN.login_redirect);
                $state.go('app.dashboard');
            };


            /*
             TODO: If accessing admin page check if role exists. Only then redirect else destroy session and redirect to 403 page.
             */
        };

        $scope.deleteSession = function () {
            Logger.log("IN DELETE SESSION");
            SessionService.deleteSession();
        };


        $scope.reLogin = function () {
            SessionService.deleteSession();
            UserService.deleteUser();
            $state.go('idn.login');
        };

        var init = function () {

            Logger.log("IN DASHBOARD INIT");
            if (SessionService.checkSession()) {

                if (SESSION_CONFIG.member) {
                    
                    navigateUserToModule();

                } else {
                    var session = SessionService.getSession();

                    UserService.setToken(session);

                    var user = UserService.getUserObject(),
                        roles = RolesService.getUserRoles();

                    $q.all([user, roles])
                        .then(function (success) {

                            UserService.setUser(success[0]);
                            UserService.setRole(success[1]);

                            Logger.log("USER OBJECT RECIEVED");
                            Logger.log(SESSION_CONFIG);

                            // if(APP_CONFIG.app["layout.modular"]){
                            //
                            // } else {
                            //     Logger.log("LAYOUT IS NOT MODULAR");
                            // };

                            LayoutService.buildLayout().then(function (laySuccess) {
                                navigateUserToModule();
                            }, function (layError) {
                                Logger.log("Error Buinding Layout");
                            });

                        }, function (error) {
                            if (error.status && error.status === 401) {
                                $scope.reLogin();
                            } else {
                                $scope.showError = true;
                            };

                        })
                }


            } else {

                if (APP_CONFIG.ACCESS_TOKEN) {

                    if (StateService.state) {

                        //no session, public page, build layout/header as guest user for display
                        $state.go(StateService.state.next.name, StateService.state.params);

                    } else {
                        $state.go('idn.login');
                    };

                } else {
                    $state.go('home');
                };

            };
        };

        init();

    }]);

/**
 * Created by nbos on 3/14/16.
 */
angular.module('mod.idn')
    .controller('LoginCtrl', ['$scope', 'LoginService', 'AlertService', 'SessionService', '$state', 'UserService', 'RolesService', 'FBService', 'GoogleService', '$mdMedia', 'LayoutService', '$q', '$accountKit', function($scope, LoginService, AlertService, SessionService, $state, UserService, RolesService, FBService, GoogleService, $mdMedia, LayoutService, $q, $accountKit) {

        $scope.showProgress = false;
        $scope.disabledLogin = false;
        $scope.user = {
            username: '',
            password: ''
        };

        var setUserData = function(responseObj) {
            UserService.setUser(responseObj.member);
            UserService.setToken(responseObj.token);


            SessionService.setSession(responseObj.token, responseObj.member.uuid);


            var roles = RolesService.getUserRoles(),
                layout = LayoutService.buildLayout();

            $q.all([roles, layout])
                .then(function(success) {
                    UserService.setRole(success[0]);
                    if (SessionService.checkSession()) {
                        $state.go('idn.dashboard');
                    };
                }, function(layError) {
                    console.log("Error Buinding Layout");
                });






        };

        $scope.validateuser = function(loginForm) {

            $scope.disabledLogin = true;
            // $scope.showProgress = true;
            LoginService.login($scope.user).then(function(success) {

                setUserData(success);

                // try {
                //
                //
                //     } catch (err) {
                //         // alert("ERROR");
                //         var message = err.statusText + ": ";
                //         for (var i = 0; i < err.data.errs.length; i++) {
                //             message += error.data.errors[i].message + ". ";
                //         }
                //         AlertService.alert(message, 'md-warn');
                //     } finally {
                //         $scope.disabledLogin = false;
                //         $scope.user = {
                //             username: '',
                //             password: ''
                //         };
                //     };
            }, function(error) {
                $scope.disabledLogin = false;
                var message = error.statusText + ": ";
                for (var i = 0; i < error.data.errors.length; i++) {
                    message += error.data.errors[i].message + ". ";
                }
                AlertService.alert(message, 'md-warn');
            });

        };

        $scope.socialLogin = function(socialType) {
            switch (socialType) {
                case 'fb':
                    FBService.fbLogin().then(function(authResponse) {
                        if (authResponse) {
                            setUserData(authResponse);
                        }
                    }, function(error) {
                        if (error) {
                            var message = error.statusText + ": ";
                            for (var i = 0; i < error.data.errors.length; i++) {
                                message += error.data.errors[i].message + ". ";
                            }
                            AlertService.alert(message, 'md-warn');
                        }
                    });
                    break;
                case 'google':
                    GoogleService.googleLogin().then(function(authResponse) {
                        if (authResponse) {
                            setUserData(authResponse);
                        }
                    }, function(error) {
                        var message = error.statusText + ": ";
                        for (var i = 0; i < error.data.errors.length; i++) {
                            message += error.data.errors[i].message + ". ";
                        }
                        AlertService.alert(message, 'md-warn');
                    });
                    break;
                case 'accountkit':
                    $scope.disabledLogin = true;
                    $accountKit.loginWithSMS()
                        .then(function(authResponse) {
                            if (authResponse && authResponse.member && authResponse.member.uuid) {
                                setUserData(authResponse);
                            }
                        }, function(error){
                            $scope.disabledLogin = false;
                            //AlertService.alert("Something Went Wrong with mobile login, Please try again later", 'md-warn');
                        });
                    break;
            }
        };

        var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && $scope.customFullscreen;

        $scope.$watch(function() {
            return $mdMedia('xs');
        }, function(wantsFullScreen) {
            if ($mdMedia('xs')) {
                console.log("setting to true");
                $scope.minWidthClass = true;
            } else {
                console.log("setting to false");
                $scope.minWidthClass = false;
            }

        });

    }]);

/**
 * Created by nbos on 3/14/16.
 */
angular.module('mod.idn')
    .controller('ResetCtrl', ['$scope', 'PasswordService', 'AlertService', '$stateParams', function($scope, PasswordService, AlertService, $stateParams) {

        $scope.reset={
            newPassword:'',
            password2:'',
            token : $stateParams.token
        };

        $scope.success = false;
        $scope.showForgot = false;

        $scope.message = '';

        var token = $stateParams.token;
        console.log(token);

        $scope.resetPassword = function(){
            if($scope.reset.newPassword === $scope.reset.password2){
                PasswordService.resetPassword($scope.reset).then(function(success){
                    $scope.success= true;
                }, function(error){
                    $scope.showForgot = true;
                });
            } else {
                AlertService.alert("Passwords don't match", "md-warn");
            }
        };

    }]);

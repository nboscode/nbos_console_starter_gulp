angular.module('mod.idn')
    .directive('socialLogins', ['CLIENT_CONFIG', function(CLIENT_CONFIG) {
        return {
            restrict: 'E',
            templateUrl: 'mod_idn/views/registration/idn.socialLogins.html',
            link: function(scope, iElement, iAttrs) {
                console.log("here")
                scope.socialArr = [];

                for (var key in CLIENT_CONFIG.SOCIAL) {
                    switch (key) {
                        case 'FACEBOOK_ID':
                            if (CLIENT_CONFIG.SOCIAL[key] != "") {
                                scope.socialArr.push({
                                    "imageUrl": "assets/icons/facebook.svg",
                                    "name": "facebook",
                                    "short_name": "fb"
                                })
                            }
                            break;
                        case 'GOOGLE_ID':
                            if (CLIENT_CONFIG.SOCIAL[key] != "") {
                                scope.socialArr.push({
                                    "imageUrl": "assets/icons/googlePlus.svg",
                                    "name": "google",
                                    "short_name": "google"
                                })
                            }
                            break;
                        case 'ACCOUNTKIT_ID':
                            if (CLIENT_CONFIG.SOCIAL[key] != "") {
                                scope.socialArr.push({
                                    "imageUrl": "assets/icons/facebook.svg",
                                    "name": "accountkit",
                                    "short_name": "accountkit"
                                })
                            }
                            break;

                    }
                }
            }
        };
    }])

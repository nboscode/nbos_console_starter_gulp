angular.module('mod.idn')
    //ALL SERVICE CALLS
    .factory('PasswordService', ['PasswordFactory', '$q', function(PasswordFactory, $q) {
        var factory = {};
        factory.changePassword = function(user) {
            var deferred = $q.defer();
            IDN_SDK.update_password().authenticate(user).$promise.then(function(response) {
                deferred.resolve(response);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        };

        factory.forgotPassword = function(email){
            var deferred = $q.defer();

            IDN_SDK.forgotPassword().forgot({email : email}, function(success){
                deferred.resolve(success);
            }, function(error){
                deferred.reject(error);
            });

            return deferred.promise;
        };

        factory.resetPassword = function(resetObject){
            var deferred = $q.defer();

            IDN_SDK.resetPassword().reset(resetObject, function(success){
                deferred.resolve(success);
            }, function(error){
                deferred.reject(error);
            });

            return deferred.promise;
        };

        return factory;
    }]);

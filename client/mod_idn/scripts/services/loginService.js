angular.module('mod.idn')
        //ALL SERVICE CALLS
        .factory('LoginService', ['IDN_SDK', '$q', 'SessionService', 'IDN_CONSTANTS', function(IDN_SDK, $q, SessionService, IDN_CONSTANTS){

            var factory = {};

            factory.login  = function(user){

                var deferred = $q.defer();

                IDN_SDK.user_authenticate().authenticate(user).$promise.then(function(response){

                    deferred.resolve(response);

                }, function(error){


                    deferred.reject(error);

                });

                return deferred.promise;
            };


            factory.signUp = function(user){

                var deferred = $q.defer();

                IDN_SDK.register_user().save(user).$promise.then(function(res){
                    if(res.status == IDN_CONSTANTS.INTERNAL_ERROR || res.status == IDN_CONSTANTS.INVALID_DETAILS){
                        deferred.reject(res);
                    } else {
                        deferred.resolve(res);
                    };
                }, function(error){
                    deferred.reject(error);
                });

                return deferred.promise;
            };

            factory.logout = function(){
                var deferred = $q.defer();

                IDN_SDK.user_logout().logout({}, function(success){
                    SessionService.deleteSession();
                    deferred.resolve(success);
                }, function(error){
                    deferred.reject(error);
                });

                return deferred.promise;
            };

            return factory;
        }]);

angular.module('mod.idn').
factory('SocialLogin', function($resource, MOD_IDN, APP_CONFIG, SessionService) {
    return function() {
        var bearer = "Bearer " + APP_CONFIG.ACCESS_TOKEN;
        var accessToken = SessionService.getSession().accessToken;
        return $resource(MOD_IDN.API_URL + 'auth/social/:socialUrl/connect', {
            socialUrl: '@socialUrl',
        }, {
            'login': {
                method: 'POST',
                headers: {
                    "Authorization": bearer
                }
            },
            'link': {
                method: 'POST',
                headers: {
                    'Authorization': 'Bearer ' + accessToken
                }
            }
        });
    };
});
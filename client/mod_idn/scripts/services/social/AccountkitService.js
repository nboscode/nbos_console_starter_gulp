//loading sdk
var fileref = document.createElement('script');
fileref.setAttribute("type", "text/javascript");
fileref.setAttribute("src", "https://sdk.accountkit.com/en_US/sdk.js");
if (typeof fileref != "undefined")
    document.getElementsByTagName("head")[0].appendChild(fileref);

//adding hidden Field

//creating form
var form = document.createElement('form');
form.setAttribute("method", "post");
form.setAttribute("id", "accountKitForm");
form.setAttribute("name", "accountKitForm");
form.setAttribute("style", "display:none;");
document.getElementsByTagName("body")[0].appendChild(form);

// creating fields
var codeInput = document.createElement('input');
codeInput.setAttribute('type', 'text');
codeInput.setAttribute('name', 'accountKitCode');
codeInput.setAttribute('id', 'accountKitCode');

var stateInput = document.createElement('input');
stateInput.setAttribute('type', 'text');
stateInput.setAttribute('name', 'accountKitState');
stateInput.setAttribute('id', 'what');

var submitInput = document.createElement('input');
submitInput.setAttribute('type', 'submit');
submitInput.setAttribute('value', 'Submit');

document.getElementById("accountKitForm").appendChild(codeInput);
document.getElementById("accountKitForm").appendChild(stateInput);
document.getElementById("accountKitForm").appendChild(submitInput);


AccountKit_OnInteractive = function() {
    angular.element(document.body).injector().get("$accountKit").init();
};

(function() {
    angular.module('nbosAccountKit', [])
        .service('$accountKit', $AccountKit)
        .provider("accountKit", AccountKitProvider);

    $AccountKit.$inject = ['accountKit', '$q', 'SocialLogin', '$httpParamSerializer'];
    AccountKitProvider.$inject = [];

    function $AccountKit(accountKit, $q, SocialLogin, $httpParamSerializer) {

        this.init = function() {
            AccountKit.init({
                appId: accountKit.app_id,
                state: accountKit.state,
                version: accountKit.api_version
            });
            console.log("sdk loaded");

        }

        this.loginWithSMS = function(countryCode, phoneNumber) {
            var deferred = $q.defer();
            var params = {};
            if (countryCode)
                params.countryCode = countryCode;
            if (phoneNumber)
                params.phoneNumber = phoneNumber;
            if (Object.keys(AccountKit) && Object.keys(AccountKit).length && Object.keys(AccountKit).indexOf('login') >= 0) {
                AccountKit.login("PHONE", params, function(response) {
                    if (response.status === "PARTIALLY_AUTHENTICATED") {
                        document.getElementById("accountKitCode").value = response.code;
                        document.getElementById("accountKitState").value = response.state;
                        var credentials = {};
                        credentials.clientId = "0f1de920-789a-4257-aad6-240eefa5921e";
                        credentials.code = response.code;
                        //credentials.expiresIn = response.authResponse.expiresIn;
                        SocialLogin().accountKit({
                            socialUrl: 'accountKit',
                            connect: 'authorize'
                        }, $httpParamSerializer(credentials)).$promise.then(function(auth) {
                                deferred.resolve(auth);
                            },
                            function(error) {
                                deferred.reject(error);
                            });
                    } else if (response.status === "NOT_AUTHENTICATED") {
                        deferred.reject("Authenticatio Failed");
                    } else if (response.status === "BAD_PARAMS") {
                        deferred.reject("Bad parameters to login function");
                    }
                });
            } else {
                deferred.reject({ "error": "Something went wrong" });
            }
            return deferred.promise;
        };

        this.loginWithEmail = function(emailAddress) {
            var params = {};
            if (emailAddress)
                params.emailAddress = emailAddress;
            AccountKit.login("EMAIL", params, this.loginCallback);
            return deferred.promise;
        };

        this.loginCallback = function(response) {

        };
    }

    function AccountKitProvider() {
        var app_id, api_version, state, callbackUrl;
        return {
            configure: function(_app_id, _api_version, _state, _callbackUrl) {
                app_id = _app_id;
                api_version = _api_version;
                state = _state;
                callbackUrl = _callbackUrl;
            },
            $get: function() {
                return {
                    app_id: app_id,
                    api_version: api_version,
                    state: state,
                    callbackUrl: callbackUrl
                }
            }
        }
    }
})();

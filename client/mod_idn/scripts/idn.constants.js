'use strict';

angular.module('mod.idn')
    .constant('IDN_CONSTANTS_2', {
        INVALID_DETAILS : 500,
        INTERNAL_ERROR : "VALIDATION ERROR"
    });
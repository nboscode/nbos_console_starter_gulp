'use strict';

angular.module('client', [
    //Add Client Dependencies Here

    //App Dependencies
    'app.layout',
    'app.theme',
    'mod.idn',
    'mod.app',
    'nbosAccountKit'
])

.constant('CLIENT_CONFIG', {
        CLIENT_ID: '8eccea72-48cd-4988-8255-e881309d2725',
        CLIENT_SECRET: 'websecret',
        CLIENT_DOMAIN: 'http://www.yourdomain.com',
        GRANT_TYPE: "client_credentials",
        SCOPE: "",
        TENANT_ID: "TNT:MYA-tq2ibmak",
        SOCIAL: {
            FACEBOOK_ID: "258042741255551",
            GOOGLE_ID: "",
            ACCOUNTKIT_ID: ""
        }
    })
    .config(function(accountKitProvider, CLIENT_CONFIG) {
        var clientId = "";
        if(CLIENT_CONFIG.SOCIAL.ACCOUNTKIT_ID == ""){
            clientId = "0000"
        }else{
            clientId = CLIENT_CONFIG.SOCIAL.ACCOUNTKIT_ID;
        }
        accountKitProvider.configure(clientId, "v1.1", "aaa", "#/idn/login");
    });

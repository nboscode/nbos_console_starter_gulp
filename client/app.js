'use strict';

angular.module('clientApp', [
    //Dependencies
    'ngAnimate',
    'ngResource',
    'ui.router',
    'ngMaterial',
    'LocalStorageModule',
    'angular-loading-bar',
    'ngMessages',
    'underscore',
    'ngFileUpload',

    //NBOS
    'app.core',
    'client'
])

    /*
     NBOS NOTE:

     Configuring the local storage for the application.
     */
    .config(['localStorageServiceProvider', 'CLIENT_CONFIG', function (localStorageServiceProvider, CLIENT_CONFIG) {
        localStorageServiceProvider
            .setPrefix("HELLO_WORLD");

    }]);
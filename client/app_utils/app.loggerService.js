angular.module('app.core')
    .factory('Logger', ['APP_CONFIG', function(APP_CONFIG){

        var factory = {};

        factory.log = function(message){
            if(APP_CONFIG.ENV === 'development')
                console.log(message);
        };

        factory.info = function(message){
            if(APP_CONFIG.ENV === 'development')
                console.info(message);
        };

        factory.warn = function(message){
            if(APP_CONFIG.ENV === 'development')
                console.warn(message);
        };

        return factory;

    }]);
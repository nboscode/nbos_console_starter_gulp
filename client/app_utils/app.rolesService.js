'use strict';

angular.module('app.core')
    .factory('RolesService', ['IDN_SDK', '$q', 'APP_CONFIG', 'SESSION_CONFIG', function(IDN_SDK, $q, APP_CONFIG, SESSION_CONFIG){

        var factory = {};
        factory.roles = null;

        factory.getUserRoles = function(){
            var deferred = $q.defer();
            deferred.resolve(true);
            return deferred.promise;
        };

        /*todo: waiting for ROles API*/

        factory.getUserRoles = function(){
            var deferred = $q.defer();

            IDN_SDK.get_user_roles().get({tenantId:APP_CONFIG.TENANT_ID, uuid:SESSION_CONFIG.UUID}, function(success){
                console.log("SUCCESS IN ROLESSERVICE");
                deferred.resolve(success);
            }, function(error){
                console.log("ERROR IN ROLESSERVICE");
                deferred.reject(error);
            });

            // IDN_SDK.getUserObject().get().$promise.then(function(success){
            //     deferred.resolve(success);
            // }, function(error){
            //     deferred.reject(error);
            // });

            return deferred.promise;

        };

        factory.get_all_roles = function(){
            var deferred = $q.defer();

            IDN_SDK.getAllRoles().get({tenantId:CLIENT_CONFIG.TENANT_ID}, function(success){
                factory.roles = success;
                deferred.resolve(success);
            }, function(error){
                deferred.reject(error);
            } );

            return deferred.promise;
        };

        return factory;
    }])
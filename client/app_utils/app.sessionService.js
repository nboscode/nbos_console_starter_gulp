/**
 * Created by nbos on 3/14/16.
 *
 * vrathore, 3/14/16
 * always store accesstoken in cookies only.
 */
angular.module('app.core')
    .service('SessionService', ['localStorageService', 'CLIENT_CONFIG', 'SESSION_CONFIG', function(localStorageService, CLIENT_CONFIG, SESSION_CONFIG){

        this.setSession = function(session, uuid){
            if(localStorageService.isSupported){
                localStorageService.set(CLIENT_CONFIG.TENANT_ID + "USER_ACCESS_TOKEN", session.access_token);
                localStorageService.cookie.set(CLIENT_CONFIG.TENANT_ID + "USER_ACCESS_TOKEN", session.access_token);
                localStorageService.set(CLIENT_CONFIG.TENANT_ID + "REFRESH_TOKEN", session.refresh_token);
                localStorageService.set(CLIENT_CONFIG.TENANT_ID + "TOKEN_TYPE", session.token_type);
                localStorageService.set(CLIENT_CONFIG.TENANT_ID + "EXPIRES_IN", session.expires_in);
                localStorageService.set(CLIENT_CONFIG.TENANT_ID + "SCOPE", session.scope);
                localStorageService.set(CLIENT_CONFIG.TENANT_ID + "UUID", uuid);
            } else {
                localStorageService.cookie.set(CLIENT_CONFIG.TENANT_ID + "USER_ACCESS_TOKEN", session.access_token);
                localStorageService.cookie.set(CLIENT_CONFIG.TENANT_ID + "REFRESH_TOKEN", session.refresh_token);
                localStorageService.cookie.set(CLIENT_CONFIG.TENANT_ID + "TOKEN_TYPE", session.token_type);
                localStorageService.cookie.set(CLIENT_CONFIG.TENANT_ID + "EXPIRES_IN", session.expires_in);
                localStorageService.cookie.set(CLIENT_CONFIG.TENANT_ID + "SCOPE", session.scope);
                localStorageService.cookie.set(CLIENT_CONFIG.TENANT_ID + "UUID", uuid);
            }
        };

        this.getSession = function(){
            var session = {};

            if(localStorageService.isSupported){
                session.access_token = localStorageService.get(CLIENT_CONFIG.TENANT_ID + "USER_ACCESS_TOKEN");
                session.refresh_token = localStorageService.get(CLIENT_CONFIG.TENANT_ID + "REFRESH_TOKEN");
                session.token_type = localStorageService.get(CLIENT_CONFIG.TENANT_ID + "TOKEN_TYPE");
                session.expires_in = localStorageService.get(CLIENT_CONFIG.TENANT_ID + "EXPIRES_IN");
                session.scope = localStorageService.get(CLIENT_CONFIG.TENANT_ID + "SCOPE");
                session.uuid = localStorageService.get(CLIENT_CONFIG.TENANT_ID + "UUID");
            } else {
                session.access_token = localStorageService.cookie.get(CLIENT_CONFIG.TENANT_ID + "USER_ACCESS_TOKEN");
                session.refresh_token = localStorageService.cookie.get(CLIENT_CONFIG.TENANT_ID + "REFRESH_TOKEN");
                session.token_type = localStorageService.cookie.get(CLIENT_CONFIG.TENANT_ID + "TOKEN_TYPE");
                session.expires_in = localStorageService.cookie.get(CLIENT_CONFIG.TENANT_ID + "EXPIRES_IN");
                session.scope = localStorageService.cookie.get(CLIENT_CONFIG.TENANT_ID + "SCOPE");
                session.uuid = localStorageService.cookie.get(CLIENT_CONFIG.TENANT_ID + "UUID");
            };


            SESSION_CONFIG.token = session;
            SESSION_CONFIG.ACCESS_TOKEN = session.access_token;
            SESSION_CONFIG.UUID = session.uuid;

            return session;


        };

        this.checkSession = function(){
            if(localStorageService.isSupported) {
                if(localStorageService.get(CLIENT_CONFIG.TENANT_ID + "USER_ACCESS_TOKEN")){
                    return true;
                } else {
                    return false;
                };
            } else {
                if(localStorageService.cookie.get(CLIENT_CONFIG.TENANT_ID + "USER_ACCESS_TOKEN")){
                    return true;
                } else {
                    return false;
                };
            }

        };

        this.getStoredUserToken = function(){
            if(localStorageService.isSupported) {
                return localStorageService.get(CLIENT_CONFIG.TENANT_ID + "USER_ACCESS_TOKEN");
            } else {
                return localStorageService.cookie.get(CLIENT_CONFIG.TENANT_ID + "USER_ACCESS_TOKEN");
            }
        };

        this.deleteSession = function(){

            if(localStorageService.isSupported){
                localStorageService.clearAll();
            };

            localStorageService.cookie.clearAll();
        };

        return this;
    }]);

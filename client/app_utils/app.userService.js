'use strict';

angular.module('app.core')
    .factory('UserService', ['IDN_SDK', '$q', 'SESSION_CONFIG', 'SessionService', function(IDN_SDK, $q, SESSION_CONFIG, SessionService) {


        var factory = {};

        factory.member;

        // factory.token;

        //TODO: retrieve 'user access token' for all calls for modules from UserService not SessionService

        factory.setUser = function(val) {

            SESSION_CONFIG.member = val;
            SESSION_CONFIG.UUID = val.uuid;

        };

        factory.setToken = function(val) {

            SESSION_CONFIG.token = val;
            SESSION_CONFIG.USER_TOKEN = val.access_token;

        };

        factory.setRole = function(val) {

            SESSION_CONFIG.role = val;

        };

        factory.checkUserObj = function() {
            return factory.member ? true : false;
        };

        factory.updateUserImg = function(id, file) {




            var deferred = $q.defer();
            var bearer = "Bearer " + SessionService.getStoredUserToken();


            IDN_SDK.update_user_image(id, file).then(function(success){
                deferred.resolve(data);
            }, function(error){
                deferred.reject(error);
            });

            return deferred.promise;
        };

        factory.getUserImg = function(id) {
            var deferred = $q.defer();

            IDN_SDK.get_user_image().get({ 'id': id, 'mediafor': 'profile' }, function(success) {
                deferred.resolve(success);
            }, function(error) {

                deferred.reject(error);
            });

            return deferred.promise;

        };

        factory.searchUser = function(obj) {
            var deferred = $q.defer();

            IDN_SDK.tenantUser().get(obj, function(success) {
                deferred.resolve(success);
            }, function(error) {

                deferred.reject(error);
            });

            return deferred.promise;

        }

        factory.getTenantUsers = function(tenantId) {
            var deferred = $q.defer();

            IDN_SDK.tenantUser().get({ tenantId: tenantId }, function(success) {
                deferred.resolve(success);
            }, function(error) {

                deferred.reject(error);
            });

            return deferred.promise;

        }

        factory.deleteUser = function() {
            console.log("IN DELETE USER");
            factory.member = null;
            factory.token = null;
        };

        factory.getUserObject = function() {
            var deferred = $q.defer();
            var session = SessionService.getSession();

            IDN_SDK.get_user().get({ userId: session.uuid }, function(success) {
                factory.member = {};
                SESSION_CONFIG.member = {};

                for (var key in success) {
                    if (key[0] != "$") {
                        SESSION_CONFIG.member[key] = success[key];
                        factory.member[key] = success[key];
                    };
                };

                SESSION_CONFIG.session = SessionService.getSession();

                console.log(SESSION_CONFIG);
                deferred.resolve(success);
            }, function(error) {

                deferred.reject(error);
            });

            return deferred.promise;

        };

        factory.updateUser = function(id, profile) {
            var deferred = $q.defer();
            var session = SessionService.getSession();

            IDN_SDK.getUserObject().update({ userId: id }, profile, function(success) {
                factory.member = {};
                SESSION_CONFIG.member = {};

                for (var key in success) {
                    if (key[0] != "$") {
                        SESSION_CONFIG.member[key] = success[key];
                        factory.member[key] = success[key];
                    };
                };

                SESSION_CONFIG.session = SessionService.getSession();

                console.log(SESSION_CONFIG);
                deferred.resolve(success);
            }, function(error) {
                deferred.reject(error);
            });

            return deferred.promise;

        }

        /*
         * Written in Node API 
         */
        factory.getNodeProfile = function() {
            var deferred = $q.defer();
            var session = SessionService.getSession();
            IDN_SDK.userProfile().get({ userId: session.uuid }, function(success) {
                factory.nodeMember = {};
                SESSION_CONFIG.nodeMember = {};

                for (var key in success) {
                    if (key[0] != "$") {
                        SESSION_CONFIG.nodeMember[key] = success[key];
                        factory.nodeMember[key] = success[key];
                    };
                };
                deferred.resolve(success);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;

        };

        factory.updateNodeProfile = function(profile) {
            var deferred = $q.defer();

            IDN_SDK.userProfile().update(profile, function(success) {
                factory.nodeMember = {};
                SESSION_CONFIG.nodeMember = {};

                for (var key in success) {
                    if (key[0] != "$") {
                        SESSION_CONFIG.nodeMember[key] = success[key];
                        factory.nodeMember[key] = success[key];
                    };
                };
                deferred.resolve(success);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;

        };

        return factory;
    }]);

'use strict';

angular.module('app.core')

    .factory('ErrorService', ['$state', '$mdMedia', '$mdDialog', function($state, $mdMedia, $mdDialog){
        var factory = {};
        factory.error;
        factory.inState;

        factory.setError = function(error, inState){
            factory.error = error;
            factory.inState = inState;

        };

        /**
         *
         * @param error
         * @param clickOutsideToClose
         * @param showLinks
         *
         * @description
         * # showLinks boolean, shows home and login links to navigate to
         * # error Object, the error object
         * # clickOutsideToClose boolean, self explanatory
         */
        factory.showError = function(error, clickOutsideToClose, showLinks){
            var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'));
            $mdDialog.show({
                controller: function($scope, error, clickOutsideToClose, showLinks){
                    $scope.error = error;
                    $scope.clickOutsideToClose = clickOutsideToClose;
                    $scope.showLinks = showLinks;

                    $scope.answer = function(answer) {
                        $mdDialog.hide(answer);
                    };
                },
                locals:{
                    error: error,
                    clickOutsideToClose : clickOutsideToClose,
                    showLinks : showLinks
                },
                templateUrl: 'app_utils/error_template.html',
                parent: angular.element(document.body),
                clickOutsideToClose:clickOutsideToClose,
                fullscreen: useFullScreen
            }).then(function(answer) {
                $state.go(answer);
            }, function() {
                $state.go('home');
            });
        };

        factory.reset = function(){
            factory.error = null;
            factory.inState = null;
        };

        return factory;
    }]);
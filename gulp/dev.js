var gulp = require('gulp');

gulp.task('start', function() {
  // place code for your default task here
});

gulp.task('set-dev', function() {
    return process.env.NODE_ENV = 'development';
});

gulp.task('set-prod', function() {
    return process.env.NODE_ENV = 'production';
});


gulp.task('clean', function () {
  return;
});

gulp.task('dev', ['styles', 'server', 'watch', 'watch_idn', 'watch_layout', 'watch_app']);
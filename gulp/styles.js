'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');


gulp.task('styles', function() {
  gulp.src('client/assets/sass/Main.scss')
      .pipe(sass().on('error', sass.logError))
      .pipe(concat('appStyles.css'))
      .pipe(gulp.dest('client/assets/css/'))
});
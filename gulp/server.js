'use strict';

var gulp = require('gulp');
var nodemon = require('gulp-nodemon');

gulp.task('server', function () {
    nodemon({
        script: './launch.js',
        ignore: '*',
        env: { 'NODE_ENV': 'development' }
    })
});
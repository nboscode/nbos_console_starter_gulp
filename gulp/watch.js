'use strict';

var gulp = require('gulp');

//Watch task
gulp.task('watch',function() {
  gulp.watch('client/assets/sass/**/*.scss',['styles']);
});

gulp.task('watch_idn',function() {
    gulp.watch('client/mod_idn/assets/sass/**/*.scss',['styles']);
});

gulp.task('watch_layout',function() {
    gulp.watch('client/mod_layout/*.scss',['styles']);
});

gulp.task('watch_app',function() {
    gulp.watch('client/mod_app/**/*.scss',['styles']);
});
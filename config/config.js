module.exports = {
    client_id     :   "11cbe195-0a88-43e6-be7a-a207ab518a0b",
    client_secret :   "testSecret",
    grant_type    :   "client_credentials",
    scope         :   "",
    tenant_id     :   "TNT:UPI-a0iwhd1l",

    //If this is true, you must set values in client/client.js
    authenticateTokenAtClient: false,

    development : {
        port : 9010
    },

    production : {
        port : 8080
    },

    //DO NOT FILL THIS
    token : {
        access_token : '',
        token_type : '',
        expires_in :''
    }
};
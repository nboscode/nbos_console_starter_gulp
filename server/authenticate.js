var request = require('request');
var Q = require('q');

module.exports = {
    
    authenticateToken : function(token){
        var defer = Q.defer();
        
        var options = {
            url : 'http://api.qa1.nbos.in/oauth/token'
        }

        request({
            url: 'http://api.qa1.nbos.in/oauth/token',
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            form : token
        }, function(error, response, body){
            if(error) {
                defer.reject(error);
            } else {
                if(response.statusCode === 200){
                    defer.resolve(JSON.parse(body));
                } else {
                    defer.reject(JSON.parse(body));
                };
            }
        });

        return defer.promise;
    }
    
}
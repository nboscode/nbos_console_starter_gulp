var CONFIG = require('../config/config');


exports.index = function (req, res) {

    /*
    //Uncomment this code if you want to use subdomains in your app.
    var domain = req.headers.host,
        subDomain = domain.split('.');

    if (subDomain[0] === "admin") {
        res.render('admin', { subDomain: "admin", ENV: process.env.APP_ENV });
    } else {
        res.render('index', { subDomain: "app", ENV: process.env.APP_ENV });
    };
    */

    var env = process.env.APP_ENV || 'development'; 
    
    res.render('index', { ENV: env, ACCESS_TOKEN :  CONFIG.token.access_token, TENANT_ID : CONFIG.tenant_id });


};


/**
 * Module dependencies.
 */


module.exports = {

	startServer: function () {

		var express = require('express'),
			routes = require('./routes'),
			pageNotFound = require('./pageNotFound'),
			path = require('path'),
			morgan = require('morgan'),
			fs = require('fs'),
			favicon = require('serve-favicon'),
			errorhandler = require('errorhandler'),
			serveStatic = require('serve-static'),
			CONFIG = require('../config/config');

		var app = express();
		var accessLogStream = fs.createWriteStream(__dirname + '/app.log',{flags: 'a'});
		var ENV = process.env.NODE_ENV || 'development';
		var appRoot = __dirname.slice(0, -7); // removing /server from the __dirname value
		console.log(appRoot);

		// all environments
		app.set('port', CONFIG[ENV].port || 9001);
		app.set('views', appRoot + '/views');
		app.set('view engine', 'ejs');
		app.use(favicon(appRoot + '/client/assets/favicon/favicon.ico'));
		app.use(morgan('combined', {stream: accessLogStream}));
		app.use(serveStatic(appRoot + '/client'));
		app.use('/site', serveStatic(appRoot + '/website'));

		// development only
		if (process.env.NODE_ENV === 'development') {

			app.use(errorhandler());

		};

		//For the Angular App
		app.get('/', routes.index);

		//For Other Routes
		app.get('*', pageNotFound.index);


		app.listen(app.get('port'), function () {
			console.log('Express server listening on port ' + app.get('port'));
		});
	}
}


